#!/bin/bash -e
###############################################################################

hash ln
hash shuf

###############################################################################

BITSERVICES_WALLPAPER_SOURCES="/usr/share/backgrounds/Bitservices Wallpaper Sources"
BITSERVICES_WALLPAPER_SYMLINK="/usr/share/backgrounds/Bitservices Wallpaper/random.wallpaper"

###############################################################################

BITSERVICES_WALLPAPER_SELECTED="$(shuf -n1 -e "${BITSERVICES_WALLPAPER_SOURCES}"/*)"

###############################################################################

ln -fv "${BITSERVICES_WALLPAPER_SELECTED}" "${BITSERVICES_WALLPAPER_SYMLINK}"

###############################################################################
